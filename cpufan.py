#!/usr/bin/env python3
from time import sleep
import RPi.GPIO as gpio
import sys

triggerH = 49.0
triggerM = 44.0
triggerL = 39.0
changed = 0
state = 0
switches = 0
DAEMON = True
range = [
    0,
    50,
    100
]

delay = .5
relayPin = 25
gpio.setmode(gpio.BCM)
gpio.setup(relayPin, gpio.OUT)
gpio.setwarnings(False)


def checkTemp():
    with open("/sys/class/thermal/thermal_zone0/temp") as file:
        return(float(file.read()) / 1000)


pwm = gpio.PWM(relayPin, 30)  # init and PWN and set Frequency to 30hz
pwm.start(range[0])
print(F"Starting Temp: {checkTemp()}")

if len(sys.argv) == 2:
    for arg in sys.argv:
        if arg == "-v":
            DAEMON = False

try:
    while True:
        temp = checkTemp()
        if temp > triggerH and state < 2:
            state = 2
            changed = 1
        elif state == 2 and temp < (triggerH - 2):
            state = 1
            changed = 1
        elif temp > triggerM and state < 1:
            state = 1
            changed = 1
        elif temp <= triggerL and state != 0:
            state = 0
            changed = 1
        if changed:
            pwm.ChangeDutyCycle(range[state])
            changed = 0
            switches += 1
        if not DAEMON:
            print(F"\rTemp: {temp:2.2f} State:{state} Changed:{changed} "
                  F"Switches: {switches}", end="")
        sleep(delay)
except KeyboardInterrupt:
    print("\nBYE")
finally:
    gpio.cleanup()
