#!/usr/bin/env python3
from time import sleep
import RPi.GPIO as gpio
import sys

config = {
    'temps': {
        'max': 49,
        'min': 35,
    },
    'relayPin': 25
}


def checkTemp():
    with open("/sys/class/thermal/thermal_zone0/temp") as file:
        return(float(file.read()) / 1000)


def main():
    gpio.setmode(gpio.BCM)
    gpio.setup(config.relayPin, gpio.OUT)
    gpio.setwarnings(False)  # Prevent warning when restarting.
    range = config.temps.max - config.temps.min
    pwm = gpio.PWM(config.relayPin, 30)
    pwm.start(20)
    while True:
        cur_temp = checkTemp()
        percentage = int(cur_temp - config.temps.max / range) * 100
        pwm.ChangeDutyCycle(percentage)


if __name__ == '__main__':
    main()
